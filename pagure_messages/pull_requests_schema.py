# Copyright (C) 2020  Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from .base import SCHEMA_URL, pagureMessage, PROJECT, PULL_REQUEST


class PullRequestNewV1(pagureMessage):
    """
    A sub-class of a Fedora message that defines a message schema for messages
    published by pagure when a new thing is created.
    """

    topic = "pagure.pull-request.new"

    body_schema = {
        "id": SCHEMA_URL + topic,
        "$schema": "http://json-schema.org/draft-04/schema#",
        "description": "Schema for messages sent when a new project is created",
        "type": "object",
        "properties": {
            "agent": {"type": "string"},
            "pullrequest": PULL_REQUEST,
        },
        "required": ["agent", "pullrequest"],
    }

    def __str__(self):
        """Return a complete human-readable representation of the message."""
        return "New Pull-Request: {fullname}#{id}\nBy: {agent}".format(
            fullname=self.body["pullrequest"]["project"]["fullname"],
            id=self.body["pullrequest"]["id"],
            agent=self.body["agent"],
        )

    @property
    def summary(self):
        """Return a summary of the message."""
        return "{agent} opened a pull-request {fullname}#{id}: {title}".format(
            agent=self.body["agent"],
            fullname=self.body["pullrequest"]["project"]["fullname"],
            id=self.body["pullrequest"]["id"],
            title=self.body["pullrequest"]["title"],
        )


class PullRequestCommentAddedV1(pagureMessage):
    """
    A sub-class of a Fedora message that defines a message schema for messages
    published by pagure when a new thing is created.
    """

    topic = "pagure.pull-request.comment.added"

    body_schema = {
        "id": SCHEMA_URL + topic,
        "$schema": "http://json-schema.org/draft-04/schema#",
        "description": "Schema for messages sent when a new project is created",
        "type": "object",
        "properties": {
            "agent": {"type": "string"},
            "pullrequest": PULL_REQUEST,
        },
        "required": ["agent", "pullrequest"],
    }

    def __str__(self):
        """Return a complete human-readable representation of the message."""
        return "Pull-Request: {fullname}#{id} has a new comment\nBy: {agent}".format(
            fullname=self.body["pullrequest"]["project"]["fullname"],
            id=self.body["pullrequest"]["id"],
            agent=self.body["agent"],
        )

    @property
    def summary(self):
        """Return a summary of the message."""
        return "{agent} commented on the pull-request {name}#{id}".format(
            agent=self.body["agent"],
            name=self.body["pullrequest"]["project"]["fullname"],
            id=self.body["pullrequest"]["id"],
        )


class PullRequestClosedV1(pagureMessage):
    """
    A sub-class of a Fedora message that defines a message schema for messages
    published by pagure when a new thing is created.
    """

    topic = "pagure.pull-request.closed"

    body_schema = {
        "id": SCHEMA_URL + topic,
        "$schema": "http://json-schema.org/draft-04/schema#",
        "description": "Schema for messages sent when a new project is created",
        "type": "object",
        "properties": {
            "agent": {"type": "string"},
            "pullrequest": PULL_REQUEST,
            "merged": {"type": "boolean"},
        },
        "required": ["agent", "pullrequest", "merged"],
    }

    def __str__(self):
        """Return a complete human-readable representation of the message."""
        return "Pull-Request: {fullname}#{id} has been {action}\nBy: {agent}".format(
            fullname=self.body["pullrequest"]["project"]["fullname"],
            id=self.body["pullrequest"]["id"],
            agent=self.body["agent"],
            action="merged" if self.body["merged"] else "closed without merging",
        )

    @property
    def summary(self):
        """Return a summary of the message."""
        return "{agent} {action} the pull-request {name}#{id}".format(
            agent=self.body["agent"],
            name=self.body["pullrequest"]["project"]["fullname"],
            id=self.body["pullrequest"]["id"],
            action="merged" if self.body["merged"] else "closed without merging",
        )
