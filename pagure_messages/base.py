# Copyright (C) 2020  Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from fedora_messaging import message
from fedora_messaging.schema_utils import user_avatar_url


SCHEMA_URL = "http://fedoraproject.org/message-schema/"


TagColored = {
    "type": "object",
    "properties": {
        "tag": {"type": "string"},
        "tag_description": {"type": "string"},
        "tag_color": {"type": "string"},
    },
    "required": ["tag", "tag_description", "tag_color"],
}


BOARD = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "active": {"type": "boolean"},
        "status": {"type": ["array", "null"], "items": {"type": "string"}},
        "tag": {"type": TagColored},
    },
    "required": ["name", "active", "status", "tag"],
}

BOARD_STATUS = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "bg_color": {"type": "string"},
        "close": {"type": "boolean"},
        "close_status": {"type": ["string", "null"]},
        "default": {"type": "boolean"},
    },
    "required": ["name", "bg_color", "close", "close_status", "default"],
}


BOARD_ISSUE = {
    "type": "object",
    "properties": {
        "board": {"type": BOARD},
        "status": {"type": BOARD_STATUS},
        "rank": {"type": "number"},
    },
    "required": ["board", "rank", "status"],
}


USER = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "fullname": {"type": "string"},
        "url_path": {"type": "string"},
    },
    "required": ["name", "fullname", "url_path"],
}


PAGURE_LOG = {
    "type": "object",
    "properties": {
        "id": {"type": "number"},
        "type": {"type": "string"},
        "ref_id": {"type": "string"},
        "date": {"type": "string"},
        "date_created": {"type": "string"},
        "user": USER,
    },
    "required": ["id", "type", "ref_id", "date", "date_created", "user"],
}


MILESTONES = {
    "type": "object",
    "properties": {
        "date": {"type": "string"},
        "active": {"type": "boolean"},
    },
}


PRIORITIES = {
    "type": "object",
    "properties": {
        "date": {"type": "string"},
        "active": {"type": "boolean"},
    },
}


BASE_PROJECT = {
    "type": "object",
    "properties": {
        "id": {"type": "number"},
        "name": {"type": "string"},
        "fullname": {"type": "string"},
        "url_path": {"type": "string"},
        "description": {"type": "string"},
        "namespace": {"type": ["string", "null"]},
        "parent": {"type": "null"},
        "date_created": {"type": "string"},
        "date_modified": {"type": "string"},
        "user": USER,
        # "access_users": {"type": "string"},
        # "access_groups": {"type": "string"},
        "tags": {"type": "array", "items": {"type": "string"}},
        # "priorities": {"type": "object"},
        "custom_keys": {
            "type": "array",
            "items": {"type": "array", "items": {"type": "string"}},
        },
        "close_status": {"type": "array", "items": {"type": "string"}},
        "milestones": {"type": "object", "properties": {"type": MILESTONES}},
    },
    "required": [
        "id",
        "name",
        "fullname",
        "url_path",
        "description",
        "namespace",
        "parent",
        "date_created",
        "date_modified",
        "user",
        # "access_users",
        # "access_groups",
        "tags",
        # "priorities",
        "custom_keys",
        "close_status",
        "milestones",
    ],
}


PROJECT = {
    "type": "object",
    "properties": {
        "id": {"type": "number"},
        "name": {"type": "string"},
        "fullname": {"type": "string"},
        "url_path": {"type": "string"},
        "description": {"type": "string"},
        "namespace": {"type": ["string", "null"]},
        # "parent": {"type": ["null", BASE_PROJECT]},
        "date_created": {"type": "string"},
        "date_modified": {"type": "string"},
        "user": USER,
        # "access_users": {"type": "string"},
        # "access_groups": {"type": "string"},
        "tags": {"type": "array", "items": {"type": "string"}},
        # "priorities": {"type": "object"},
        "custom_keys": {
            "type": "array",
            "items": {"type": "array", "items": {"type": "string"}},
        },
        "close_status": {"type": "array", "items": {"type": "string"}},
        "milestones": {
            "oneOf": [
                {"type": "null"},
                MILESTONES,
            ]
        },
    },
    "required": [
        "id",
        "name",
        "fullname",
        "url_path",
        "description",
        "namespace",
        # "parent",
        "date_created",
        "date_modified",
        "user",
        # "access_users",
        # "access_groups",
        "tags",
        # "priorities",
        "custom_keys",
        "close_status",
        "milestones",
    ],
}


RELATED_PR = {
    "type": "object",
    "properties": {
        "id": {"type": "number"},
        "title": {"type": "string"},
    },
    "required": ["id", "title"],
}


ISSUE = {
    "type": "object",
    "properties": {
        "id": {"type": "number"},
        "title": {"type": "string"},
        "content": {"type": "string"},
        "status": {"type": "string"},
        "close_status": {"type": ["string", "null"]},
        "date_created": {"type": "string"},
        "last_updated": {"type": "string"},
        "closed_at": {"type": ["string", "null"]},
        "user": USER,
        "private": {"type": "boolean"},
        "tags": {"type": "array", "items": {"type": "string"}},
        "depends": {"type": "array", "items": {"type": "string"}},
        "blocks": {"type": "array", "items": {"type": "string"}},
        "assignee": {"oneOf": [{"type": "null"}, USER]},
        # "priorities": {"type": "object"},
        "milestone": {
            "oneOf": [
                {"type": "null"},
                MILESTONES,
            ]
        },
        "custom_fields": {
            "type": "array",
            "items": {"type": "array", "items": {"type": "string"}},
        },
        "closed_by": {"oneOf": [{"type": "null"}, USER]},
        "related_prs": {
            "oneOf": [{"type": "null"}, {"type": "array", "items": RELATED_PR}]
        },
    },
    "required": [
        "id",
        "title",
        "content",
        "status",
        "close_status",
        "date_created",
        "last_updated",
        "closed_at",
        "user",
        "private",
        "tags",
        "depends",
        "blocks",
        "assignee",
        # "priorities",
        "milestone",
        "custom_fields",
        "closed_by",
        "related_prs",
    ],
}


PULL_REQUEST = {
    "type": "object",
    "properties": {
        "id": {"type": "number"},
        "uid": {"type": "string"},
        "title": {"type": "string"},
        "branch": {"type": "string"},
        "project": PROJECT,
        "branch_from": {"type": "string"},
        "repo_from": {"oneOf": [{"type": "null"}, USER]},
        "remote_git": {"oneOf": [{"type": "null"}, {"type": "string"}]},
        "date_created": {"type": "string"},
        "updated_on": {"type": "string"},
        "last_updated": {"type": "string"},
        "closed_at": {"oneOf": [{"type": "null"}, {"type": "string"}]},
        "user": USER,
        "assignee": {"oneOf": [{"type": "null"}, USER]},
        "status": {"type": "string"},
        "commit_start": {"oneOf": [{"type": "null"}, {"type": "string"}]},
        "commit_stop": {"oneOf": [{"type": "null"}, {"type": "string"}]},
        "closed_by": {"oneOf": [{"type": "null"}, USER]},
        "initial_comment": {"type": "string"},
        "cached_merge_status": {"type": "string"},
        "threshold_reached": {"oneOf": [{"type": "null"}, {"type": "string"}]},
        "tags": {"type": "array", "items": {"type": "string"}},
    },
    "required": [
        "id",
        "uid",
        "title",
        "branch",
        "project",
        "branch_from",
        "repo_from",
        "remote_git",
        "date_created",
        "updated_on",
        "last_updated",
        "closed_at",
        "user",
        "assignee",
        "status",
        "commit_start",
        "commit_stop",
        "closed_by",
        "initial_comment",
        "cached_merge_status",
        "threshold_reached",
        "tags",
    ],
}


COMMIT_FLAG = {
    "type": "object",
    "properties": {
        "commit_hash": {"type": "string"},
        "username": {"type": "string"},
        "percent": {"type": "string"},
        "comment": {"type": "string"},
        "status": {"type": "string"},
        "url": {"type": "string"},
        "date_created": {"type": "string"},
        "date_updated": {"type": "string"},
        "user": USER,
    },
    "required": [
        "commit_hash",
        "username",
        "percent",
        "comment",
        "status",
        "url",
        "date_created",
        "date_updated",
        "user",
    ],
}


GROUP = {
    "type": "object",
    "properties": {
        "display_name": {"type": "string"},
        "description": {"type": "string"},
        "creator": USER,
        "members": {"type": "array", "items": {"type": "string"}},
        "date_created": {"type": "string"},
        "group_type": {"type": "string"},
        "name": {"type": "string"},
    },
    "required": [
        "display_name",
        "description",
        "creator",
        "members",
        "date_created",
        "group_type",
        "name",
    ],
}


def _get_project(content, key="project"):
    """Return the project as `foo` or `user/foo` if the project is a fork."""
    project = content[key]["name"]
    ns = content[key].get("namespace")
    if ns:
        project = "/".join([ns, project])
    if content[key]["parent"]:
        user = content[key]["user"]["name"]
        project = "/".join(["fork", user, project])
    return project


class pagureMessage(message.Message):
    """
    A sub-class of a Fedora message that defines a message schema for messages
    published by pagure.
    """

    __link__ = "https://pagure.io"
    __stg_link__ = "https://stg.pagure.io"

    @property
    def app_name(self):
        return "pagure"

    @property
    def app_icon(self):
        return "https://apps.fedoraproject.org/img/icons/pagure.png"

    @property
    def agent(self):
        return self.body.get("agent")

    @property
    def agent_avatar(self):
        return user_avatar_url(self.agent)

    @property
    def usernames(self):
        return [self.agent]

    @property
    def url(self):
        try:
            project = _get_project(self.body)
        except KeyError:
            try:
                project = _get_project(self.body["pullrequest"])
            except KeyError:
                project = "(unknown)"

        base_url = self.__link__
        if ".stg." in self.topic:
            base_url = self.__stg_link__

        tmpl = "{base_url}/{project}"
        if "pagure.issue" in self.topic:
            issueid = self.body["issue"]["id"]
            if "comment.edited" in self.topic:
                # This is for an edited issue..
                comment = self.body.get("comment")
                comment_id = None
                if comment:
                    comment_id = comment.get("id")
                if comment_id and project != "(unknown)":
                    tmpl += "/issue/{id}#comment-{comment}"
                    return tmpl.format(
                        base_url=base_url,
                        project=project,
                        id=issueid,
                        comment=comment_id,
                    )
                elif project != "(unknown)":
                    tmpl += "/issue/{id}"
                    return tmpl.format(base_url=base_url, project=project, id=issueid)
                else:
                    return base_url
            elif "comment.added" in self.topic:
                comments = self.body["issue"]["comments"]
                if comments:
                    tmpl += "/issue/{id}#comment-{comment}"
                    return tmpl.format(
                        base_url=base_url,
                        project=project,
                        id=issueid,
                        comment=comments[-1]["id"],
                    )
                else:
                    tmpl += "/issue/{id}"
                    return tmpl.format(base_url=base_url, project=project, id=issueid)
            else:
                tmpl += "/issue/{id}"
                return tmpl.format(base_url=base_url, project=project, id=issueid)
        elif "pagure.pull-request" in self.topic:
            key = "pullrequest"
            for k in ["pullrequest", "pull_request", "pull-request"]:
                if k in self.body:
                    key = k
                    break
            prid = self.body[key]["id"]
            if "comment" in self.topic:
                comments = self.body["pullrequest"]["comments"]
                if comments:
                    tmpl += "/pull-request/{id}#comment-{comment}"
                    return tmpl.format(
                        base_url=base_url,
                        project=project,
                        id=prid,
                        comment=comments[-1]["id"],
                    )
                elif self.body.get("comment"):
                    comment_id = self.body["comment"]["id"]
                    tmpl += "/pull-request/{id}#comment-{comment}"
                    return tmpl.format(
                        base_url=base_url,
                        project=project,
                        id=prid,
                        comment=comment_id,
                    )
                else:
                    tmpl += "/pull-request/{id}"
                    return tmpl.format(base_url=base_url, project=project, id=prid)
            else:
                tmpl += "/pull-request/{id}"
                return tmpl.format(base_url=base_url, project=project, id=prid)
        elif "pagure.request" in self.topic:
            prid = self.body["request"]["id"]
            tmpl += "/pull-request/{id}"
            return tmpl.format(base_url=base_url, project=project, id=prid)
        elif "pagure.project.deleted" in self.topic:
            return base_url
        elif "pagure.project" in self.topic:
            return tmpl.format(base_url=base_url, project=project)
        elif "pagure.commit.flag" in self.topic:
            tmpl += "/c/{commit_hash}"
            project = _get_project(self.body, "repo")
            commit_hash = self.body["flag"]["commit_hash"]
            return tmpl.format(
                base_url=base_url, project=project, commit_hash=commit_hash
            )
        elif "pagure.git.receive" in self.topic:
            if "commit" in self.body:
                project = _get_project(self.body["commit"], key="repo")
                item = self.body["commit"]["rev"]
                tmpl += "/{item}"
            else:
                project = _get_project(self.body, key="repo")
                item = self.body["branch"]
                if "refs/heads/" in item:
                    item = item.replace("refs/heads/", "")
                tmpl += "/tree/{item}"
            return tmpl.format(base_url=base_url, project=project, item=item)

        else:
            return base_url
